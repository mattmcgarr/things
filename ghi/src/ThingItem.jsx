import React from "react";
import { useDeleteThingMutation } from "./services/things";

const ThingItem = ({id, name}) => {
    const [deleteThing] = useDeleteThingMutation()
    return (
        <li>
            {name}
            {` `}
            <button className="btn btn-sm btn-danger" onClick={(e) => deleteThing(id)}>Delete</button>
        </li>
    )
}

export default ThingItem;
